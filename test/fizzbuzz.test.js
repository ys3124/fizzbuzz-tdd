"use strict";

const assert = require("assert").strict;
const FizzBuzz = require("../fizzbuzz");

describe("FizzBuzzClass test", () => {
  let fizzbuzz;
  beforeEach("前準備", () => {
    fizzbuzz = new FizzBuzz();
  });
  describe("convertメソッドは数を文字列に変換する", () => {
    describe("3 の倍数の時は数の代わりに「Fizz」に変換する", () => {
      it("3 を渡すと文字列'Fizz'を返す", () => {
        assert.equal(fizzbuzz.convert(3), "Fizz");
      });
    });
    describe("5 の倍数の時は数の代わりに「Buzz」に変換する", () => {
      it("5 を渡すと文字列'Buzz'を返す", () => {
        assert.equal(fizzbuzz.convert(5), "Buzz");
      });
    });
    describe("その他の数の時はそのまま文字列に変換する", () => {
      it("1 を渡すと文字列'1'を返す", () => {
        assert.equal(fizzbuzz.convert(1), "1");
      });
    });
  });
});
